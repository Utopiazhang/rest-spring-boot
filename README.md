# Rest spring boot

 a REST service backed by H2 Database (in-memory SQL), implemented by Spring Boot2.0


How to start:

import the project to Intellij and run it.

Or use commands in terminal:

java -jar assignment-0.0.1.jar


API:

data domain 1: Employee
1. insert: ​http://localhost:8080/hr/insert
2. search by id: ​http://localhost:8080/hr/search?id=123
3. search all data in the hr table: ​http://localhost:8080/hr/search/all

 
data domain 2: Car
1. insert: ​http://localhost:8080/car/insert
2. search by id: ​http://localhost:8080/car/search?vin=123
3. search all data in the hr table: ​http://localhost:8080/car/search/all


data domain 3: Movie
1. insert: ​http://localhost:8080/movie/insert
2. search by id: ​http://localhost:8080/movie/search?id=123
3. search all data in the hr table: ​http://localhost:8080/movie/search/all
            

