package com.zhongjian.zhang.assignment.controller;

import com.zhongjian.zhang.assignment.domain.Movie;
import com.zhongjian.zhang.assignment.service.interf.MovieServiceInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class MovieController {
    @Resource(name = "movieService")
    MovieServiceInterface movieServiceInterface;

    /**
     * bind the insert page with a endpoint
     */
    @RequestMapping(value = "movie/insert", method = RequestMethod.GET)
    public ModelAndView insertPage(){
        return new ModelAndView("movie/insert");
    }


    /**
     * insert data
     */
    @RequestMapping(value = "movie/insert/do", method = RequestMethod.POST)
    public Movie insert(Movie movie){
        return movieServiceInterface.insert(movie.getId(),movie.getName(),movie.getDirector());
    }

    /**
     * search and return in the inserted data
     * url: http://localhost:8080/movie/search?id=123
     */
    @RequestMapping(value = "movie/search")
    public List<Movie> search(int id){
        return  movieServiceInterface.searchById(id);
    }
    /**
     * search and return all in the inserted data
     */
    @RequestMapping(value = "movie/search/all", method = RequestMethod.GET)
    public List<Movie> getAll(){
        return  movieServiceInterface.getAll();
    }
}
