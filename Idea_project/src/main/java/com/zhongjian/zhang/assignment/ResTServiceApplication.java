package com.zhongjian.zhang.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResTServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResTServiceApplication.class, args);
    }

}
