package com.zhongjian.zhang.assignment.domain;

import com.zhongjian.zhang.assignment.utils.MyConstant;

import javax.persistence.*;

/**
 * domain Employee
 */
@Entity
@Table(name = MyConstant.HR_TABLE_NAME)
public class Employee {
    /**
     * identifier
     */
    @Id
//    @GeneratedValue(generator = "guidGenerator")
//    @GenericGenerator(name = "guidGenerator", strategy = "uuid")
//    @Column(name = "UUID", unique = true, nullable = false)
//    private String uuid;
    @Column(name = MyConstant.HR_COLUM_KEY, unique = true, nullable = false)
    private int id;

    /**
     * additional field 1
     */
    @Column(name = MyConstant.HR_COLUM_1)
    private String firstName;

    /**
     * additional field 2
     */
    @Column(name = MyConstant.HR_COLUM_2)
    private String lastName;

    /**
     * additional field 3
     */
    @Column(name = MyConstant.HR_COLUM_3)
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

