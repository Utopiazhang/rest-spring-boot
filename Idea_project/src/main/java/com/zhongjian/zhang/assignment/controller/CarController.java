package com.zhongjian.zhang.assignment.controller;



import com.zhongjian.zhang.assignment.domain.Car;
import com.zhongjian.zhang.assignment.service.interf.CarServiceInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class CarController {

    @Resource(name = "carService")
    CarServiceInterface carServiceInterface;

    /**
     * bind the insert page with a endpoint
     */
    @RequestMapping(value = "car/insert", method = RequestMethod.GET)
    public ModelAndView insertPage(){
        return new ModelAndView("car/insert");
    }


    /**
     * insert data
     */
    @RequestMapping(value = "car/insert/do", method = RequestMethod.POST)
    public Car insert(Car car){
        return carServiceInterface.insert(car.getVin(),car.getColor(),car.getWeight());
    }

    /**
     * search and return in the inserted data
     * url: http://localhost:8080/car/search?id=123
     */
    @RequestMapping(value = "car/search")
    public List<Car> search(int vin){
        return  carServiceInterface.searchByVin(vin);
    }
    /**
     * search and return all in the inserted data
     */
    @RequestMapping(value = "car/search/all", method = RequestMethod.GET)
    public List<Car> getAll(){
        return carServiceInterface.getAll();
    }
}
