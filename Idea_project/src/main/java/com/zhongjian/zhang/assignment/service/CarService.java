package com.zhongjian.zhang.assignment.service;

import com.zhongjian.zhang.assignment.dao.CarDao;
import com.zhongjian.zhang.assignment.domain.Car;
import com.zhongjian.zhang.assignment.service.interf.CarServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService implements CarServiceInterface {
    @Autowired
    CarDao carDao;

    /**
     * insert data
     */
    @Override
    public Car insert(int vin, String color, float weight) {
        Car Car = new Car();
        Car.setVin(vin);
        Car.setColor(color);
        Car.setWeight(weight);

        try {
            return carDao.save(Car);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                //DuplicateKeyException be caught, but no alert for now.
                throw e;
            }
            return null;
        }
    }
    /**
     * search in the inserted data
     */
    @Override
    public List<Car> getAll() {
        return carDao.findAll();
    }

    @Override
    public List<Car> searchByVin(int vin) {
        return carDao.findAllByVin(vin);
    }
}
