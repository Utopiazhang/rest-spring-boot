package com.zhongjian.zhang.assignment.controller;

import com.zhongjian.zhang.assignment.domain.Employee;
import com.zhongjian.zhang.assignment.service.interf.HRServiceInterface;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class HRController {

    @Resource(name = "HRService")
    HRServiceInterface hrServiceInterface;

    /**
     * bind the insert page with a endpoint
     */
    @RequestMapping(value = "hr/insert", method = RequestMethod.GET)
    public ModelAndView insertPage(){
        return new ModelAndView("hr/insert");
    }


    /**
     * insert data
     */
    @RequestMapping(value = "hr/insert/do", method = RequestMethod.POST)
    public Employee insert(Employee employee){
        System.out.println(employee.getFirstName());
        return hrServiceInterface.insert(employee.getId(),employee.getFirstName(),employee.getLastName(),employee.getEmail());
    }

    /**
     * search and return in the inserted data
     * url: http://localhost:8080/hr/search?id=123
     */
    @RequestMapping(value = "hr/search")
    public List<Employee> search(int id){
        return  hrServiceInterface.searchById(id);
    }
    /**
     * search and return all in the inserted data
     */
    @RequestMapping(value = "hr/search/all", method = RequestMethod.GET)
    public List<Employee> getAll(){
        return  hrServiceInterface.getAll();
    }
}
