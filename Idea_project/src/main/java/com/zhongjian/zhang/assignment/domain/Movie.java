package com.zhongjian.zhang.assignment.domain;

import com.zhongjian.zhang.assignment.utils.MyConstant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * domain Employee
 */
@Entity
@Table(name = MyConstant.MOVIE_TABLE_NAME)
public class Movie {
    /**
     * identifier
     */
    @Id
//    @GeneratedValue(generator = "guidGenerator")
//    @GenericGenerator(name = "guidGenerator", strategy = "uuid")
//    @Column(name = "UUID", unique = true, nullable = false)
//    private String uuid;
    @Column(name = MyConstant.MOVIE_COLUM_KEY, unique = true, nullable = false)
    private int id;

    /**
     * additional field 1
     */
    @Column(name = MyConstant.MOVIE_COLUM_1)
    private String name;

    /**
     * additional field 2
     */
    @Column(name = MyConstant.MOVIE_COLUM_2)
    private String director;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }
}

