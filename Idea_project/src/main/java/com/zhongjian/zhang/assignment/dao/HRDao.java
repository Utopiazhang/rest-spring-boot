package com.zhongjian.zhang.assignment.dao;
import com.zhongjian.zhang.assignment.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * The Interface for access Employee table
 */
public interface HRDao extends JpaRepository<Employee, String>, JpaSpecificationExecutor<Employee>{

    /**
     * search by id
     */
    public List<Employee> findAllById(int id);

}
