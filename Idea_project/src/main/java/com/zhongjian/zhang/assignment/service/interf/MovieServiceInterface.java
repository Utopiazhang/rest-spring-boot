package com.zhongjian.zhang.assignment.service.interf;


import com.zhongjian.zhang.assignment.domain.Movie;

import java.util.List;

public interface MovieServiceInterface {
    /**
     * insert data
     */
    public Movie insert(int id, String name, String director);

    /**
     * search in the inserted data
     */
    public List<Movie> getAll();

    /**
     * search by id
     */
    public List<Movie> searchById(int id);

}
