package com.zhongjian.zhang.assignment.service;

import com.zhongjian.zhang.assignment.dao.MovieDao;
import com.zhongjian.zhang.assignment.domain.Movie;
import com.zhongjian.zhang.assignment.service.interf.MovieServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieService implements MovieServiceInterface {
    @Autowired
    MovieDao movieDao;

    /**
     * insert data
     */
    @Override
    public Movie insert(int id, String name, String director) {
        Movie movie = new Movie();
        movie.setId(id);
        movie.setName(name);
        movie.setDirector(director);

        try {
            return movieDao.save(movie);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                //DuplicateKeyException be caught, but no alert for now.
                throw e;
            }
            return null;
        }
    }
    /**
     * search in the inserted data
     */
    @Override
    public List<Movie> getAll() {
        return movieDao.findAll();
    }

    @Override
    public List<Movie> searchById(int id) {
        return movieDao.findAllById(id);
    }
}
