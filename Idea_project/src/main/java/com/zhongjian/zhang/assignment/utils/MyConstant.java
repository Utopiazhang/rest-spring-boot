package com.zhongjian.zhang.assignment.utils;

public interface MyConstant {
    String HR_TABLE_NAME="TBL_HR";
    String HR_COLUM_KEY = "ID";
    String HR_COLUM_1 = "FIRST_NAME";
    String HR_COLUM_2 = "LAST_NAME";
    String HR_COLUM_3 = "EMAIL";

    String CAR_TABLE_NAME="TBL_CAR";
    /**
     * vehicle identification number
     */
    String CAR_COLUM_KEY = "VIN";
    String CAR_COLUM_1 = "COLOR";
    String CAR_COLUM_2 = "WEIGHT";

    String MOVIE_TABLE_NAME="TBL_MOVIE";
    String MOVIE_COLUM_KEY = "ID";
    String MOVIE_COLUM_1 = "NAME";
    String MOVIE_COLUM_2 = "DIRECTOR";

}
