package com.zhongjian.zhang.assignment.dao;
import com.zhongjian.zhang.assignment.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * The Interface for access Employee table
 */
public interface CarDao extends JpaRepository<Car, String>, JpaSpecificationExecutor<Car>{

    /**
     * search by vin
     */
    public List<Car> findAllByVin(int vin);

}
