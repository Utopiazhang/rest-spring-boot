package com.zhongjian.zhang.assignment.service;

import com.zhongjian.zhang.assignment.dao.HRDao;
import com.zhongjian.zhang.assignment.domain.Employee;
import com.zhongjian.zhang.assignment.service.interf.HRServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HRService implements HRServiceInterface {
    @Autowired
    HRDao hrDao;

    /**
     * insert data
     */
    @Override
    public Employee insert(int id, String firstName, String lastName, String email) {
        Employee employee = new Employee();
        employee.setId(id);
        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setEmail(email);

        try {
            return hrDao.save(employee);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                //DuplicateKeyException be caught, but no alert for now.
                throw e;
            }
            return null;
        }
    }
    /**
     * search in the inserted data
     */
    @Override
    public List<Employee> getAll() {
        return hrDao.findAll();
    }

    @Override
    public List<Employee> searchById(int id) {
        return hrDao.findAllById(id);
    }
}
