package com.zhongjian.zhang.assignment.service.interf;

import com.zhongjian.zhang.assignment.domain.Employee;

import java.util.List;

public interface HRServiceInterface {
    /**
     * insert data
     */
    public Employee insert(int id, String firstName, String lastNmae, String email);

    /**
     * search in the inserted data
     */
    public List<Employee> getAll();

    /**
     * search by id
     */
    public List<Employee> searchById(int id);

}
