package com.zhongjian.zhang.assignment.service.interf;

import com.zhongjian.zhang.assignment.domain.Car;


import java.util.List;

public interface CarServiceInterface {
    /**
     * insert data
     */
    public Car insert(int vin, String color, float weight);

    /**
     * search in the inserted data
     */
    public List<Car> getAll();

    /**
     * search by id
     */
    public List<Car> searchByVin(int vin);

}
