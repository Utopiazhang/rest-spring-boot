package com.zhongjian.zhang.assignment.dao;
import com.zhongjian.zhang.assignment.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * The Interface for access Employee table
 */
public interface MovieDao extends JpaRepository<Movie, String>, JpaSpecificationExecutor<Movie>{

    /**
     * search by id
     */
    public List<Movie> findAllById(int id);

}
