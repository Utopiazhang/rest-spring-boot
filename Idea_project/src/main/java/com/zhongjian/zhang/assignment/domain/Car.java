package com.zhongjian.zhang.assignment.domain;

import com.zhongjian.zhang.assignment.utils.MyConstant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * domain Employee
 */
@Entity
@Table(name = MyConstant.CAR_TABLE_NAME)
public class Car {
    /**
     * identifier
     */
    @Id
//    @GeneratedValue(generator = "guidGenerator")
//    @GenericGenerator(name = "guidGenerator", strategy = "uuid")
//    @Column(name = "UUID", unique = true, nullable = false)
//    private String uuid;
    @Column(name = MyConstant.CAR_COLUM_KEY, unique = true, nullable = false)
    private int vin;//vehicle identification number


    /**
     * additional field 1
     */
    @Column(name = MyConstant.CAR_COLUM_1)
    private String color;

    /**
     * additional field 2
     */
    @Column(name = MyConstant.CAR_COLUM_2)
    private float weight;

    public int getVin() {
        return vin;
    }

    public void setVin(int vin) {
        this.vin = vin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}

